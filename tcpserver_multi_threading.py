#!/usr/bin/python

import socket
import threading

#This function handle the request from client
def responseToClient(s1):
    while True:
        data = s1.recv(1024)
        if data:
            print('Got message:'+`data`)
            s1.sendall('retuen message:'+`data`)

#This is main function
if __name__ == '__main__':
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  
    s.bind(('127.0.0.1', 8000))
    arrayThread = []

    while True:
        s.listen(4)
        s1, addr = s.accept()
        objThreading = threading.Thread(target=responseToClient, args=(s1,))
        objThreading.start()
        arrayThread.append(objThreading)

for item in arrayThread:
    item.join()


